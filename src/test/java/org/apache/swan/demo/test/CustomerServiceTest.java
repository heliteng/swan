package org.apache.swan.demo.test;

import org.apache.swan.demo.model.Customer;
import org.apache.swan.demo.service.CustomerService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by kenneth on 2017/6/28.
 */
public class CustomerServiceTest {
    private final CustomerService customerService;

    public CustomerServiceTest() {
        customerService = new CustomerService();
    }
    @Before
    public void init() {
        //TODO　初始化数据库

    }

    @Test
    public void getCuetomerListTest() {
        List<Customer> customerList = customerService.getCustomerList();
        Assert.assertEquals(2, customerList.size());
    }

    @Test
    public void getCuetomerTest() {
        long id=1;
        Customer customer = customerService.getCustomer(id);
        Assert.assertNotNull(customer);
    }

    @Test
    public void createrCuetomerTest() {
        Map<String,Object> fieldMap = new HashMap<String,Object>();
        fieldMap.put("name","customer1");
        fieldMap.put("contact","Join");
        fieldMap.put("telephone","13657736223");
        boolean result = customerService.createCustomer(fieldMap);
        Assert.assertTrue(result);
    }
    @Test
    public void updateCuetomerTest() {
        long id=1;
        Map<String,Object> fieldMap = new HashMap<String,Object>();
        fieldMap.put("contact","Kenneth");
        boolean result = customerService.updateCustomer(fieldMap);
        Assert.assertTrue(result);
    }

    @Test
    public void deleteCuetomerTest() {
        long id=1;
        boolean result = customerService.deleteCustomer(id);
        Assert.assertTrue(result);
    }
}
